# SGP40 driver for nrf5340dk

## Motivation

The main purpose of this driver frimware is to provide easy to use interface for nrf5340 microcntroller and the SGP40 sensor.

## Project Environment

- OS: UBUNTU
- IDE: VS Code
- DK: nrf5340-DK
- nrf connect sdk version: west v2.5.1

### What is SGP40?

The SGP40 is a digital gas sensor designed for easy integration into air purifiers or demand-controlled
ventilation systems. Its I2C interface provides several preconfigured I2C addresses while maintaining
an ultra-low power budget. 

Key features:

- 4th Generation sensor
- High-Accuracy
- Ultra-Low-Power
- Accuracies voc = ±15 %
- VDD = 1.7 V … 3.6 V
- Avg. current: 4mA, Idle current: 34 uA
- Operating range: 0 … 65535 ppm

### Datasheet for SGP40
Datasheet for SGP40 can be downloaded directly from the following  link:
https://sensirion.com/media/documents/296373BB/6203C5DF/Sensirion_Gas_Sensors_Datasheet_SGP40.pdf


## Project Structure

```mermaid
  graph LR;
  main(main)<-->measure;
  measure<-->sgp40;
  sgp40<-->sensirion_I2C;
  sensirion_common<-->sensirion_I2C;
  sensirion_config<-->sensirion_I2C;
  sensirion_I2C<-->hal_i2c;
  hal_i2c<-->5430dk;           
```

### How i2c protocol works:

I2C communcation has 2 main lines 
- SDA for data communication
- SCL for clocking

In addion the VDD and VSS(GND) should be connected between the master and the slave. 

### Development kit

nrf-5340 DK was used to communicate with the sensor:

![Alt text](assets/pcb.png "dev board")


### Sensor kit

![Alt text](assets/sgp40.jpeg "SENSOR")

## Connection Diagram

| nrf-5340 DK | SHT40   |
|-------------|---------|
| GND         | GND     |
| VDD         | VDD/3V6 |
| P1.02       | SDA     |
| P1.03       | SCL     |


### Sample Output:

``` c

*** Booting nRF Connect SDK v2.5.1 ***


 ***** SGP40 driver for nrf5340dk_nrf5340_cpuapp *****

serial: 0x000003fa079d

SRAW VOC: 30687
SRAW VOC: 30687
SRAW VOC: 30692
SRAW VOC: 30696
SRAW VOC: 30694
SRAW VOC: 30701
SRAW VOC: 30687
SRAW VOC: 30697
SRAW VOC: 30697
SRAW VOC: 30700
SRAW VOC: 30707
SRAW VOC: 30719
SRAW VOC: 30708

```

### logic analyzer output

- Sensor address

![Alt text](assets/serial_number.png "Sensor address information")

- Sensor raw measurement data

![Alt text](assets/example_i2c_write.png "Sensor write command data")

![Alt text](assets/example_i2c_read_sensor_data.png "Sensor read command data")


