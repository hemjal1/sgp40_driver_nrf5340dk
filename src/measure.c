#include "measure.h"

void measure_and_get_sensor_data(void) {

    int16_t error = 0;
    uint16_t serial_number[3];
    uint8_t serial_number_size = 3;

    sensirion_i2c_hal_init();  // initialize sensor i2c bus

    error = sgp40_get_serial_number(serial_number, serial_number_size); // get serial number
    if (error) {
        printk("Error executing sgp40_get_serial_number(): %i\n", error);
    } else {        
        printk("serial: 0x%04x%04x%04x\n", serial_number[0], serial_number[1],serial_number[2]);
        printk("\n");
    }
    
    uint16_t default_rh = 0x8000;    // Parameters for deactivated humidity compensation:
    uint16_t default_t = 0x6666;

    for (int i = 0; i < 60; i++) {
        uint16_t sraw_voc;

        sensirion_i2c_hal_sleep_usec(1000000);

        error = sgp40_measure_raw_signal(default_rh, default_t, &sraw_voc);  // Start Measurement
        if (error) {
            printk("Error executing sgp40_measure_raw_signal(): %i\n", error);
        } else {
            printk("SRAW VOC: %u\n", sraw_voc);
        }
    }
}
