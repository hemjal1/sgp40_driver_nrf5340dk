#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>

#include "measure.h"


int main(void)
{
	while (1)
	{
		printk("\n\n ***** SGP40 driver for %s *****\n\n", CONFIG_BOARD);
		measure_and_get_sensor_data();
		k_msleep(3000);
		
	}


	return 0;
}
