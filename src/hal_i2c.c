#include "hal_i2c.h"


#define I2C1_NODE DT_NODELABEL(sgp40sensor)

static const struct i2c_dt_spec dev_i2c = I2C_DT_SPEC_GET(I2C1_NODE);

void sensirion_i2c_hal_init(void) {
  
  if (!device_is_ready(dev_i2c.bus)) {
    printk("I2C bus %s is not ready!\n\r", dev_i2c.bus->name);
    return;
  }
}

int8_t sensirion_i2c_hal_read(uint8_t address, uint8_t* data, uint16_t count) {
  
  uint8_t value[count];
  uint8_t  ret = i2c_read_dt(&dev_i2c, value, count);
  memcpy(data, value, count);
		if (ret != 0)
		{
			printk("Failed to read from I2C device address. %x \n", dev_i2c.addr);
      return STATUS_FAIL;
		}
    else return ret;
  
}

void sensirion_i2c_hal_sleep_usec(uint32_t useconds) {

  k_usleep(useconds);

}

int8_t sensirion_i2c_hal_write(uint8_t address, const uint8_t* data,uint16_t count) {

  uint8_t ret = i2c_write_dt(&dev_i2c, data, count);
		if (ret != 0)
		{
			printk("Failed to write to I2C device address. %x \n", dev_i2c.addr);
      return STATUS_FAIL;
		}
		  return STATUS_OK;
  
}
